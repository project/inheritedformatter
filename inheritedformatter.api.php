<?php

/**
 * @file
 * API documentation for the inheritedformatter module.
 */

/**
 * Defines fields that can be used to reference another node.
 *
 * This hook should return an associative array.  The keys are the fields that
 * can be used as a reference field.  The values are a definition array.  For
 * now there is just one element, nid_callback, which defines a callback function
 * that can take a field record and return a node id from it.
 *
 * There is also a corresponding alter hook.
 */
function hook_inheritedformatter_reference_field_types_info() {
  return array(
    'nodereference' => array(
      'nid_callback' => 'inheritedformatter_nid_callback_simple',
    ),
  );
}

/**
 * Define fields that can inherit from another linked node.
 *
 * This hook should return a simple array of field types that can inherit.
 *
 * There is also a corresponding alter hook.
 */
function hook_inheritedformatter_supported_field_types_info() {
  return array('text', 'number_integer');
}
